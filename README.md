# AkaiKKR cif converter version 0.4 

Copyright 2017,2021 Hiori Kino

This software includes the work that is distributed in the Apache License 2.0.

## usage

```
python cif2akaikkr.py ciffile -o outputfilename

usage: cif2akaikkr.py [-h] [-o OUTPUTFILENAME] [--go GO] [--potentialfile POTENTIALFILE]
                      [--edelt EDELT] [--ewidth EWIDTH] [--reltyp RELTYP] [--sdftyp SDFTYP]
                      [--magtyp MAGTYP] [--record RECORD] [--outtyp OUTTYP] [--bzqlty BZQLTY]
                      [--maxitr MAXITR] [--pmix PMIX]
                      inputfilename
```


## requirement 
* python 3.x
* pymatgen


* m_tspace and find_wy

m_tspace is a library for find_wy. 
Please 'make' on find_wy/ and place the executable file 'write1wy' on the 'AkaiKKRTools/' directory. 

for [find_wy](https://github.com/nim-hrkn/find_wy)

for [m_tspace](https://github.com/nim-hrkn/m_tspace)


## installation 

### local install
I recommend delverpper mode installation.
```
pip install -e .
```

## sample
```
make
```
in sample/cifsamle directory.


# Notes

## problems on many cif readers ##
Many Cif readers don't read _symmetry_Int_Tables_number, but read _symmetry_equiv_pos_as_xyz, which describes symmetry operations. But it isn't easy to prepare them by hand. 

## litecif format ##
write1wy reads _symmetry_Int_Tables_number, and the 'litecif' format is only
```
_cell_length_a 7.71
_cell_length_b 7.71
_cell_length_c 7.71
_cell_angle_alpha 90
_cell_angle_beta 90
_cell_angle_gamma 90
_symmetry_Int_Tables_number 227
loop_
    _atom_site_label
    _atom_site_fract_x
    _atom_site_fract_y
    _atom_site_fract_z
    _atom_site_Wyckoff_symbol
 Fe 0 0 0 c
 Pr 0.375 0.375 0.375  b
```
The origin of the choice is automatically selected. 

## known bug(s) ##
loop_ must be the last section in the lite cif format. 

# limitation ##
* alloy can't be handled now.

# license ###
Copyright 2017,2021 Hiori Kino

This software includes the work that is distributed in the Apache License 2.0.
