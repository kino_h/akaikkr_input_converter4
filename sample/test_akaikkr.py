import NME.AkaiKKR as kkr

# filename = "inputcard.orig"

lines = """#-------------------HEA--------------------------------------
     go.exe pot.dat
#------------------------------------------------------------
#   brvtyp     a         c/a   b/a   alpha   beta   gamma
     bcc    99999   1.00   1.00      90     90    90
#------------------------------------------------------------
#   edelt    ewidth    reltyp   sdftyp   magtyp   record
    0.0001    1.7         sra     pbe      mag    init
#------------------------------------------------------------
#   outtyp    bzqlty   maxitr   pmix
    update     10        1000     0.005
#------------------------------------------------------------
#    ntyp
      1
#------------------------------------------------------------
#   type     ncmp    rmt    field   mxl  anclr   conc
    HEA       4      1    0.000     2
    10   25
                                          11   25
                                          12   25
                                          13   25
#------------------------------------------------------------
#   natm
     1
#------------------------------------------------------------
#   atmicx                            atmtyp
     0           0           0         HEA
#------------------------------------------------------------ """

ip = kkr.Input()
ip.process_input(lines)
print(ip)

ip.dic["go"] = "dos"
filenameout = "kkrinput"
with open(filenameout, "w") as f:
    f.write("\n".join(ip.Writer()))
print(filenameout, "is made.")
