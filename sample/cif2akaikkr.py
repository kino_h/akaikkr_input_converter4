#!/usr/bin/env python

import os
import akaikkr_input_converter.akaikkr_cif_parser as kkr
import argparse


def process_arg():
    parser = argparse.ArgumentParser(
        description='AkaiKKR converter', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-o', '--outputfilename',
                        type=str, default="inputcard", help="output file name")

    setting = [["go", str, "go", "go keyword"],
               ["potentialfile", str, "pot.dat","file keyword"],
               ["edelt", float, 1e-3, "edelt keyword"],
               ["ewidth", float, 1.2, "ewidth keyword"],
               ["reltyp", str, "sra", "reltype keyword"],
               ["sdftyp", str, "pbe-asa", "sdftype keyword"],
               ["magtyp", str, "mag", "magtyp keyword"],
               ["record", str, "2nd", "record keyword"],
               ["outtyp", str, "update", "outtyp keyword"],
               ["bzqlty", int, 6, "bzqlty keyword"],
               ["maxitr", int, 300, "maxitr keyword"],
               ["pmix", float, 1e-2, "pmix keyword"]]
    for x, y, z, w in setting:
        parser.add_argument('--{}'.format(x), type=y,  default=z, help=w)

    parser.add_argument("--a_unit", default="cif", help="'cif' or XXAng or XXau")
    parser.add_argument('inputfilename', type=str)
    args = parser.parse_args()
    return args, setting

def change_length_unit(valueunit):
    if valueunit.endswith("Ang"):
        ang_value = float(valueunit.replace("Ang"))
        return ang_value
    elif valueunit.endswith("au"):
        ang_value = float(valueunit.replace("Ang"))*0.529177
        return ang_value
    else:
        return valueunit

if __name__ == '__main__':

    args,option_keys = process_arg()
    filename = args.inputfilename
    kkrinputfile = args.outputfilename

    headname, tailname = os.path.split(filename)
    datafilename = tailname.split(".")[0]
    filetype = tailname.split(".")[1]

    #   coordtype="frac" or "cart"
    coordtype = "frac"
    # coordtype="cart"
    param = {}
    arg_dic = vars(args)
    for x in option_keys:
        key = x[0]
        param[key] = arg_dic[key]
    print(param)
    inputparser = kkr.InputParser(datafilename, coordtype=coordtype, param=param)

    if filetype == "cif":
        inputparser.from_ciffile(filename)

    elif filetype == "litecif":
        inputparser.from_liteciffile(filename)

        print("NME.AkaiKKR.InputParser is called with coordtype=", coordtype)
    #  You can change kkr parameter here.

    # kkrinputfile = "kkrinput"
    inputparser.write_file(kkrinputfile, potentialfile=param["potentialfile"], a_unit=arg_dic["a_unit"])
    print(kkrinputfile, "is made.")
