
import sys
import NME.core

if __name__ == '__main__':

    filename = sys.argv[1]

    sp = NME.core.StructureParser(filename)

    lines = sp.poscarWriter()
    outputfile = "POSCAR"
    with open(outputfile, "w") as f:
        for line in lines:
            f.write(line+"\n")
        print(outputfile, "is made.")

    lines = sp.xsfWriter()
    outputfile = "newstruc.xsf"
    with open(outputfile, "w") as f:
        for line in lines:
            f.write(line+"\n")
        print(outputfile, "is made.")
